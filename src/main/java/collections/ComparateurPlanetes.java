package collections;

import java.util.Comparator;

public class ComparateurPlanetes implements Comparator<Planete>{
	
	public int compare(Planete planete1, Planete planete2) {
		return planete1.taille.compareTo(planete2.taille);
	}
	
}
