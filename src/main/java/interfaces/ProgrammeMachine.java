package interfaces;

import interfaces.IMachineALaver.Reglage;

public class ProgrammeMachine {

	public static void main(String[] args) {

		IMachineALaver maMachineALaver = new MaMachineALaver();
		System.out.println("Ouverture du hublot : " + maMachineALaver.ouvrirHublot());
		System.out.println("Programme par défaut : " + maMachineALaver.getReglage());
		maMachineALaver.setReglage(Reglage.LAVAGE);
		System.out.println("Nouveau programme sélectionné : " + maMachineALaver.getReglage());
		System.out.println("Fermeture du hublot : " + maMachineALaver.fermerHublot());
		System.out.println("Démarrage machine : " + maMachineALaver.stopStart());
		System.out.println("Ouverture du hublot : " + maMachineALaver.ouvrirHublot());

		MaMachineALaver maMachineALaver2 = new MaMachineALaver();
		maMachineALaver2.ajouterLiquide();
	}
}
