package interfaces;

public class ServiceTraducteur {
	
	public ITraducteur traducteur;
	
	public IPrononciation prononciation;
	
	public ServiceTraducteur(ITraducteur traducteur, IPrononciation prononciation) {
		this.traducteur = traducteur;
		this.prononciation = prononciation;
	}
	
	public String traduction(String mot) {
		return traducteur.traduit(mot);
	}
	
	public String prononciation(String mot) {
		return prononciation.prononce(mot);
	}

}
