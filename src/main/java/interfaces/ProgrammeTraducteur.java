package interfaces;

public class ProgrammeTraducteur {

	public static void main(String[] args) {
		TestTraducteurSansInterface ttsi = new TestTraducteurSansInterface();
		System.out.println(ttsi.testTraduction());
		
		TestTraducteur tt = new TestTraducteur(new Traducteur());
		System.out.println(tt.testTraduction());
		
		Traducteur traducteur = new Traducteur();
		Prononciation prononciation = new Prononciation();
		ServiceTraducteur serviceTraducteur = 
				new ServiceTraducteur(traducteur, prononciation);
		System.out.println("Traduction de maison = " + serviceTraducteur.traduction("maison"));
	}
}
