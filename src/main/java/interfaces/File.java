package interfaces;

public class File implements IWrite, IRead {
	
	public Boolean write(String filename, String content) {
		System.out.println("file " + filename + " avec le contenu " + content + " a été écrit");
		return true;
	}
	
	public String read(String filename) {
		return "file " + filename + " a été lu";
	}

}
