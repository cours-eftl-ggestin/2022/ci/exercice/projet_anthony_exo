package formationjava;

public class Voiture {
	
	private String marque;

	private String numeroSerie;
	
	public void rouler() {
		System.out.println("La voiture de marque " + this.marque + " roule.");
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		if (marque != null) {
			this.marque = marque;
		}
	}
	
	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public String toString() {
		return "La voiture est de marque " + this.marque 
				+ " et a le numéro de série " + this.numeroSerie;
	}

}
